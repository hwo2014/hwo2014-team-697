require "rmagick"
require "json"
require "pp"

WIDTH = 2000
HEIGHT = 2000

img = Magick::Image.new(WIDTH, HEIGHT) { self.background_color = "white"}
# data = JSON.parse(File.open("../gameinit_test.json", "r").read)["data"]
data = JSON.parse(File.open("../gameinit_usa.json", "r").read)["data"]

@track = data["race"]["track"]
@cars = data["race"]["cars"]
@race = data["race"]["raceSession"]
    
# draw track center line
x0 = @track["startingPoint"]["position"]["x"] + WIDTH/2
y0 = @track["startingPoint"]["position"]["y"] + HEIGHT/2
a0 = @track["startingPoint"]["angle"]

# draw starting line
sl_length = 40 # FIXME: calculate lanes total width
sl_x0 = Math::sin(a0*Math::PI/180.0-Math::PI/2.0)*sl_length/2.0+x0
sl_y0 = Math::cos(a0*Math::PI/180.0-Math::PI/2.0)*sl_length/2.0+y0
sl_x1 = Math::sin(a0*Math::PI/180.0+Math::PI/2.0)*sl_length/2.0+x0
sl_y1 = Math::cos(a0*Math::PI/180.0+Math::PI/2.0)*sl_length/2.0+y0

starting_line = Magick::Draw.new
starting_line.stroke = "red"
starting_line.stroke_width = 2
starting_line.line(sl_x0, sl_y0, sl_x1, sl_y1)
starting_line.draw(img)

min_x = x0
max_x = x0
min_y = y0
max_y = y0

offsets = [0] + @track["lanes"].collect {|lane| lane["distanceFromCenter"]}
colors = ["gray", "green", "purple", "blue", "red"]
# pp offsets

wps = []
ls = []

offsets.each_with_index {|offset, i|
  x = x0 + Math::sin(a0*Math::PI/180.0-Math::PI/2.0)*offset
  y = y0 + Math::cos(a0*Math::PI/180.0-Math::PI/2.0)*offset
  a = a0
  l = []
  wp = []
  puts "Starting point (#{x},#{y}) at angle #{a0} degrees at lane offset #{offset}"
  
  @track["pieces"].each_with_index {|piece, j|
    wp << [x,y] if i > 0 # save waypoints
    case piece["length"].nil?
    when false
      # straight
      line = Magick::Draw.new
      line.stroke = colors[i]
      length = piece["length"]
      a1 = a
      x1 = x + length * Math::sin(a1*Math::PI/180.0)
      y1 = y + length * Math::cos(a1*Math::PI/180.0)
  #    puts "#{i}: S #{length}, #{x.to_i}, #{y.to_i}, #{a.to_i} -> #{x1.to_i}, #{y1.to_i}, #{a1.to_i}"
#      if i > 0 then
#        puts "lane #{i-1}, piece #{j}"
#        puts "draw #{x.to_i},#{y.to_i} -> #{x1.to_i},#{y1.to_i}"
#      end
      
      line.line(x, y, x1, y1)
      line.draw(img)
    else
      # bend
      arc = Magick::Draw.new
      arc.stroke = colors[i]
      arc.fill = "none"

      # A = (x,y)
      o = piece["angle"]
      a1 = a - o
      if (o > 0) then
        r = piece["radius"] - offset
      else
        r = piece["radius"] + offset
      end
      # d = (arc length)
      length = (2*r*Math::sin(o/2.0*Math::PI/180.0)).abs
    
      # a = a
      if (o > 0) then
        # turn to right
        b = a - o
      else
        # turn to left
  #      arc.stroke = "red"
        a = a + 180
        b = a + o.abs
      end
    
      if (o > 0) then
        # turn to right
        # O = (xo, yo)
        xo = x - r * Math::cos(a*Math::PI/180.0)
        yo = y + r * Math::sin(a*Math::PI/180.0)
      
        # B = (x1,y1)
        x1 = xo + r * Math::cos(b*Math::PI/180.0)
        y1 = yo - r * Math::sin(b*Math::PI/180.0)
      else
        # turn to left
        # O = (xo, yo)
        xo = x - r * Math::cos(a*Math::PI/180.0)
        yo = y + r * Math::sin(a*Math::PI/180.0)
      
        # B = (x1,y1)
        x1 = xo + r * Math::cos(b*Math::PI/180.0)
        y1 = yo - r * Math::sin(b*Math::PI/180.0)
        a = a - 180
      end
        
  #    puts "#{i}: B #{a1-a}, #{x.to_i}, #{y.to_i}, #{a.to_i} -> #{x1.to_i}, #{y1.to_i}, #{a1.to_i}"
  #    puts "#{a1-a}, #{(x1-x).to_i}, #{(y1-y).to_i}"

      arc.stroke_dasharray(5,5)
      arc.line(x, y, x1, y1)
      if offset == 0 && false
        arc.line(x, y, xo, yo)
        arc.line(x1, y1, xo, yo)
      end
      arc.draw(img)
    end
  
    if offset == 0 #&& false
      pm_length = 20
      pm_x0 = Math::sin(a1*Math::PI/180.0-Math::PI/2.0)*pm_length/2.0+x1
      pm_y0 = Math::cos(a1*Math::PI/180.0-Math::PI/2.0)*pm_length/2.0+y1
      pm_x1 = Math::sin(a1*Math::PI/180.0+Math::PI/2.0)*pm_length/2.0+x1
      pm_y1 = Math::cos(a1*Math::PI/180.0+Math::PI/2.0)*pm_length/2.0+y1

      piece_marker = Magick::Draw.new
      piece_marker.stroke = "blue"
      piece_marker.stroke_width = 1
      piece_marker.line(pm_x0, pm_y0, pm_x1, pm_y1)
      piece_marker.draw(img)
    end
  
    x = x1
    y = y1
    a = a1 % 360
    l << length
  
    max_x = x if x > max_x
    max_y = y if y > max_y
    min_x = x if x < min_x
    min_y = y if y < min_y
  
  }
  
  puts "End point (#{x.to_i},#{y.to_i}) at angle #{a} degrees."
  puts "Lane length #{l.inject(:+).to_i}"
  wps << wp if i > 0
  ls << l if i > 0
#  pp wp

}

# add switches
offsets.slice(1, offsets.size-1).each_with_index {|lane, i|
  @track["pieces"].each_with_index {|piece, j|
    next unless piece["switch"]
    # puts "switch at lane #{i}, piece #{j}"
   
    # switch to next (if next lane!)
    if i < wps.size-1 then
      x0 = wps[i][j][0]
      y0 = wps[i][j][1]
      x1 = wps[i+1][j+1][0]
      y1 = wps[i+1][j+1][1]
      l = Math::sqrt((x0-x1)**2 + (y0-y1)**2)
    
      # puts "draw #{x0.to_i},#{y0.to_i} -> #{x1.to_i},#{y1.to_i} (next lane) (#{l})"
    
      switch = Magick::Draw.new
      switch.stroke = colors[i+1]
      switch.stroke_width = 1
      switch.line(x0, y0, x1, y1)
      switch.draw(img)
    end
    
    # switch to previous (if previous lane!)
    if i > 0 then
      x0 = wps[i][j][0]
      y0 = wps[i][j][1]
      x1 = wps[i-1][j+1][0]
      y1 = wps[i-1][j+1][1]
      l = Math::sqrt((x0-x1)**2 + (y0-y1)**2)
    
      # puts "draw #{x0.to_i},#{y0.to_i} -> #{x1.to_i},#{y1.to_i} (prev lane) (#{l})"
    
      switch = Magick::Draw.new
      switch.stroke = colors[i+1]
      switch.stroke_width = 1
      switch.line(x0, y0, x1, y1)
      switch.draw(img)
    end
    
    # FIXME: Store switch lengths
    # Note that l's only work for straights, for bends, they are even more
    # far from straight lines.
  }

}

# Build tree model and optimize routes for each lane
offsets.slice(1, offsets.size-1).each_with_index {|lane, i|
  puts "Calculating shortest flight plan for starting lane #{i}"
  @race["laps"].times {|lap|
    puts "Lap #{lap+1}"
    @track["pieces"].each_with_index {|piece, j|
      # fork if switch
      if piece["switch"]
        puts "forking at #{j}"
        
      end
    }
  }
}

puts "Track size #{(max_x-min_x).to_i}x#{(max_y-min_y).to_i}"

img = img.crop(min_x-20, min_y-20, max_x-min_x+40, max_y-min_y+40, true)
text = Magick::Draw.new
text.font_family = "helvetica"
text.pointsize = 20

text.annotate(img, 0, 0, 10, 30, "Track: #{@track["name"]}")
text.annotate(img, 0, 0, 10, 60, "Laps: #{@race["laps"]}")

img.write("trackviz-#{@track["id"]}.png")
