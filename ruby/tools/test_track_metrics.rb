require "json"

require "../lib/track_metrics"
require "pp"

data = JSON.parse(File.open("../gameinit_usa.json", "r").read)["data"]
track_data = TrackMetrics.new(data["race"])

pp track_data.pieces[4]
pp track_data.pieces[4].length(0,1)
pp track_data.pieces[4].length(1,2)
pp track_data.pieces[4].length(1,0)
pp track_data.pieces[4].length(2,1)

pp track_data.distance_to_bend(0, 0 ,0)
pp track_data.distance_to_bend(1, 0 ,0)
pp track_data.distance_to_bend(2, 0 ,0)
pp track_data.distance_to_bend(28, 0 ,0) # takasuora
pp track_data.distance_to_piece(30, 0 ,0, 0) # tokavikalta palalta maaliin
pp track_data.distance_to_piece(31, 0 ,0, 0) # vikalta palalta maaliin
pp track_data.distance_to_piece(31, 50 ,0, 0) # vikalta palalta maaliin
pp track_data.distance_to_piece(0, 1, 0, 0)

pp track_data.distance_to_end(0,0,0)
pp track_data.distance_to_end(31,99,0)
pp track_data.distance_to_end(31,100,0)