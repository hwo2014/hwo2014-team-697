# (c) Team Doge, 2014
# http://www.apache.org/licenses/LICENSE-2.0.html 

require "json"
require "socket"
require "logger"
require "pp"
require "csv"

require_relative "./track_metrics"

class DogeBot
  TICKS_PER_SECOND = 60
  MS_PER_TICK = 1/TICKS_PER_SECOND.to_f*1000
  
  attr_reader :bot_name, :race_name, :race_color
  
  def initialize(server_host, server_port = 8091, bot_name, bot_key)
    unless server_host && server_port && bot_name && bot_key
      raise "Missing server and/or bot configuration"
    end
    @bot_name = bot_name
    @bot_key = bot_key
    @server_host = server_host
    @server_port = server_port
    @race_name = nil
    @race_color = nil
    @lane = nil
    @flight_plan = nil
    
    @game_tick = nil
    @speed = 0
    @acceleration = 0
    @distance = 0
    @throttle = 0
    @turbo = nil
    @angle = 0
    @piece = 0
    @position_data = nil
    @state = :racing
    
    @pos = nil
    
    default_config = {
      telemetry: false,
      debug: false,
      visualization: false,
      loglevel: "info",
      logfile: nil,
      full_transcript: false
    }
    
    if File.exists?("config.json")
      @config = default_config.merge(JSON.parse(File.open("config.json", "r").read, {symbolize_names: true}))
    else
      @config = default_config
    end
    
    if @config[:telemetry] then
      @csv = CSV.open("telemetry.csv", "w")
      @csv << ["tick", "acceleration", "velocity", "distance", "throttle", "angle", "lane", "piece", "lap"]
    end
    
    logfile = @config[:logfile] || STDOUT
    @log = Logger.new(logfile)
    @log.datetime_format = '%Y-%m-%d %H:%M:%S'
    @log.level = case @config[:loglevel]
                 when "debug" then Logger::DEBUG
                 when "info"  then Logger::INFO
                 when "warn"  then Logger::WARN
                 when "error" then Logger::ERROR
                 when "fatal" then Logger::FATAL
                 else Logger::INFO
                 end
#    @log.level = Logger::DEBUG if @config[:debug]

    @log.info "I'm #{bot_name} and trying connect to #{server_host}:#{server_port}"
  end
  
  def join_race(track_name = nil, password = nil, car_count = nil)
    play(:join, track_name, password, car_count)
  end
  
  def create_race(track_name, password = nil, car_count = 2)
    play(:create, track_name, password, car_count)
  end
  
  def name
    @bot_name
  end
  
  def quick_race(car_count = nil)
    join_race(nil, nil, car_count)
  end

  private
  
  def transcript(msg_type, msg_data, src)
    return false unless @config[:full_transcript]
    case src
    when :from
      @log.debug "<= #{msg_type}:\n#{msg_data}"
    when :to
      @log.debug "=> #{msg_type}:\n#{msg_data}"
    else
      @log.debug "=? #{msg_type}:\n#{msg_data}"
    end
  end
  
  def play(mode, track_name, password, car_count)
    raise "Unknown play mode '#{mode}'" unless [:join, :create].include?(mode)
    begin
      @log.debug "Trying to connect to #{@server_host}:#{@server_port}" 
      @tcp = TCPSocket.open(@server_host, @server_port)
    rescue SocketError => e
      @log.fatal "Connection to #{@server_host}:#{@server_port} failed"
      @log.fatal "Got socket error: #{e}"
      raise
    end
    @log.info "Connected to #{@server_host}:#{@server_port}" 
    if (mode == :create) then
      @log.info "Creating race on track '#{track_name}' for #{car_count} racers"
      @log.info "Password for race is '#{password}'" if password
      @tcp.puts create_race_message(track_name, password, car_count)
    elsif (car_count != nil && car_count > 1)
      @log.info "Joining race on track '#{track_name}' for #{car_count} racers"
      @log.info "Using password '#{password}'" if password
      @tcp.puts join_race_message(track_name, password, car_count)
    else
      @log.info "Joining a quick test race"
      @tcp.puts join_message
    end
    react_to_messages_from_server
  end
  
  def parse_track_data(data)
    # FIXME: Implement track data parsing.
    # See trackviz for some code.
    return TrackMetrics.new(data)
  end
  
  def parse_car_positions(data, bot_name)
    data = data.select {|car| car["id"]["name"] == bot_name}.first # Assumes racer names are unique
    if @pos then
      PP.pp(data, @pos)
    elsif @config[:debug]
      @pos = File.open("carpositiondata.txt", "w")
      PP.pp(data, @pos)
    end
      
    return data
  end

  def react_to_messages_from_server
    tournament_end = false
    while json = @tcp.gets
      begin
        message = JSON.parse(json)
      rescue JSON::ParseError => e
        # log invalid JSON response and ignore
        @log.error "Received invalid JSON response:\n#{json}"
        @tcp.puts ping_message
        next
      end
      
      msg_type = message['msgType']
      msg_data = message['data']
      game_id = message['gameId']
      game_tick = message['gameTick']
      
      if (@config[:debug]) then
        transcript(msg_type, msg_data, :from)
      end
      case msg_type
        when 'carPositions'
#          @log.debug "game_tick: #{game_tick}, @game_tick: #{@game_tick}"
          unless game_tick
            @log.error "Did not get a gameTick. Last received gameTick was #{@last_real_game_tick}." unless @game_tick.nil?
            if @game_tick.nil?
              @log.debug("Assuming gameTick 0")
              @game_tick = 0
              game_tick = 0 # For some reason, very first carPositions doesn't have gameTick
            else
              # At race end, there's a carPositions without a game tick
              game_tick = @game_tick + 1 # Let's assume it's the next one.
              @log.info("Assuming gameTick #{game_tick}")
            end
          else
            @last_real_game_tick = game_tick
          end

#          @log.debug "game_tick: #{game_tick}, @game_tick: #{@game_tick}"

          ## CHECK STATE OF THE WORLD
          start_time = Time.now # for AI profiling
          
          # if cp["lap"] == -1 then @throttle = 1 # haven't passed start yet
          
          # @game_tick = 0
          # @speed = 0
          # @acceleration = 0
          # @distance = 0
          # @throttle = 0
          # @turbo = nil
          # @angle = 0
          # @state = :racing
          prev_tick = @game_tick
          @game_tick = game_tick
          
          @log.error "Current game tick is smaller than previous tick! (#{game_tick} < #{prev_tick})" if game_tick < prev_tick
          @log.error "Current game tick is same as previous tick! (#{game_tick} < #{prev_tick})" if game_tick == prev_tick
          

          prev_speed = @speed
          prev_distance = @distance
          prev_acceleration = @acceleration
          prev_throttle = @throttle
          prev_angle = @angle
          
          prev_position_data = @position_data
          @position_data = parse_car_positions(msg_data, @race_name)
          prev_position_data = @position_data if prev_position_data.nil?
          
          
          delta_tick = @game_tick - prev_tick # should be 1.
          
          if @position_data["piecePosition"]["pieceIndex"] == prev_position_data["piecePosition"]["pieceIndex"]
            # We're still on the same piece, so use server's knowledge of distance travelled
#            @log.debug("Still on same piece. current IPD #{@position_data["piecePosition"]["inPieceDistance"]}, prev #{prev_position_data["piecePosition"]["inPieceDistance"]}")
            delta_distance = @position_data["piecePosition"]["inPieceDistance"] - prev_position_data["piecePosition"]["inPieceDistance"]
          else
            # Look at track data and check how long previous piece was
            # NOTE: Handle -1 index (last piece!)
            # NOTE: We might have switched lane in the last piece?!
            delta_distance = (prev_speed+prev_acceleration)*delta_tick # FIXME FIXME FIXME FIXME
#            @log.debug("New piece. Had speed #{prev_speed} so probably covered #{delta_distance}")
          end
          @distance = prev_distance + delta_distance # total distance raced
          
          if delta_tick > 0 then
            @speed = delta_distance/delta_tick.to_f
            @acceleration = (@speed-(prev_speed || 0))/delta_tick.to_f
          else
            @speed = nil
            @acceleration = nil
          end
          
          @angle = @position_data["angle"]
          @lane = @position_data["piecePosition"]["lane"]["startLaneIndex"]
          @piece = @position_data["piecePosition"]["pieceIndex"]
          @lap = @position_data["piecePosition"]["lap"]
          
          ## DECISION MAKING TIME!
          
          if @throttle <= 0 then 
            @throttle = 0.54 
            # FIXME: match to target speed
          end
          # TODO: Should I switch lanes, like now?

          if @state == :racing then
            @tcp.puts throttle_message(@throttle)
          else
            # no-op in case of crash, dnf, finished, ...
            @tcp.puts ping_message
          end
          
          final_timer = (Time.now-start_time)*1000 # For AI profiling
          @log.warn "AI took to #{final_timer.round} ms of allowed #{MS_PER_TICK.round} ms." if final_timer > MS_PER_TICK
          
          # @csv << ["tick", "a", "v", "d", "t", "angle", "lane", "p", "lap"]
#          @log.debug "#{[@game_tick, @acceleration, @speed, @distance, @throttle, @angle, @lane, @piece, @lap]}"

          @csv << [@game_tick, @acceleration, @speed, @distance, @throttle, @angle, @lane, @piece, @lap] if @config[:telemetry]
        else
          case msg_type
            when 'join', "joinRace"
              # Server should respond with the original join message
              @log.info 'Joined a race'
            when 'createRace'
              # Undocumented: Server seems to respond with the original createRace
              @log.info "Created a race for #{msg_data["carCount"]} racers."
              if msg_data["carCount"] > 1 then
                @ﬁog.info "Waiting for other participants..."
              end
            when 'yourCar'
              @race_name = msg_data["name"]
              @log.warn "Bot had name #{@bot_name}, server gave #{@race_name}!" unless @bot_name == @race_name
              @race_color = msg_data["color"]
              # Server informs of bot's name & car colour
              @log.info "Got car colour information, no stripes :("
            when 'gameInit'
              # Server sends race information
              @log.info "Got game initialization"
              File.open("gameinit.json", "w") {|f| f << json} if @config[:debug]
#              @track_data = parse_track_data(msg_data["race"])
            when 'gameStart'
              # The race has started! (data = nil)
              @log.info 'Race started!'
            when 'crash'
              # Server sends crash message when a car gets out of track.
              crasher = msg_data["name"]
              if crasher == @race_name then
                @log.warn "Bot crashed!"
                @state = :crashed
              else
                @log.debug "#{crasher} crashed."
              end
            when 'spawn'
              # Shortly after the crash, server sends spawn message as the car is restored on the track.
              # The car is restored to the same position from where it got out of the track. Throttle will be set to zero.
              spwaner = msg_data["name"]
              if spwaner == @race_name then
                @log.info "Bot spawned back on track."
                @state = :racing
                @throttle = 0
              else
                @log.info "#{spwaner} spawned back on track"
              end
            when "lapFinished"
              # Server sends lapFinished message when a car completes a lap.
              if msg_data["car"]["name"] == @race_name then
                # Bot finished a track
                @log.info "Lap #{msg_data["lapTime"]["lap"]+1} finished in #{msg_data["lapTime"]["millis"]} ms."
                @log.info "Current ranking #{msg_data["ranking"]["overall"]}, by lap time #{msg_data["ranking"]["fastestLap"]}"
              else
                @log.debug "#{msg_data["car"]["name"]} finished a lap"
              end
            when "dnf"
              # Server sends dnf message when a car is disqualified. Reason is typically a failure to comply with the bot protocol or that the bot has disconnected.
              if msg_data["car"]["name"] == @race_name then
                @log.error "Bot got disqualified for reason '#{msg_data["reason"]}'"
                @state = :dnf
              else
                @log.info "#{msg_data["car"]["name"]} got disqualified for '#{msg_data["reason"]}'"
              end
            when "finish"
              # Server sends finish message when a car finishes the race.
              if msg_data["name"] == @race_name then
                @log.info "Bot finished race!"
                @throttle = 0
                @state = :finished
              else
                @log.info "#{msg_data["name"]} finished the race!"
              end
            when 'gameEnd'
              # When the game is over, the server sends the gameEnd messages that contains race results.
              @log.info "Race ended"
              File.open("gameend.json", "w") {|f| f << json} if @config[:debug]
            when "tournamentEnd"
              # The tournament has ended (data = nil)
              # A full race consists of to races (gameStart/gameEnd pairs), the first being a qualifier.
              @log.info "Tournament ended."
              tournament_end = true
            when 'error'
              @log.error "Server threw error: '#{msg_data}'"
            when "turboAvailable"
              # Undocumented. 
              # {"msgType"=>"turboAvailable",
              # "data"=>{"turboDurationMilliseconds"=>500.0, "turboFactor"=>3.0},
              # "gameId"=>"1ddf603e-67f4-40d8-abd2-e86f4fe25289",
              # "gameTick"=>1201}
              @log.debug "Turbo available!"
              @turbo = msg_data
              # TODO: Use turbo rait nau if possible
            else
              @log.warn "Got unhandled msgType '#{msg_type}'"
              pp message if @config[:debug]
          end
          @tcp.puts ping_message # Responding with a ping to all non-CarPositions messages. 
      end
    end
    @tcp.close
    @log.info "Connection to #{@server_host}:#{@server_port} closed"
    @log.warn "Connection closed before receiving tournamentEnd!" unless tournament_end
    @csv.close if @config[:telemetry]
  end

  def join_message
    # The server will acknowledge this by replying with the same message. 
    make_msg("join", {:name => @bot_name, :key => @bot_key})
  end
  
  def create_race_message(track_name, password, car_count)
    data = {
      botId: {
        name: @bot_name,
        key: @bot_key
      },
      trackName: track_name,
      password: password,
      carCount: car_count
    }
    make_msg(:createRace, data)
  end
  
  def join_race_message(track_name = nil, password = nil, car_count = 2)
    data = {
      botId: {
        name: @bot_name,
        key: @bot_key
      },
      carCount: car_count
    }
    data[:trackName] = track_name if track_name
    data[:password] = password if password
    
    make_msg("joinRace", data)
  end
  
  def switch_lane_message(direction)
    direction = direction.to_s.capitalize
    raise "Unknown lane switching direction '#{direction}'" unless ["Left", "Right"].include?(direction) 
    make_msg("switchLane", direction)
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msg_type, data, game_tick = nil)
    unless game_tick
      msg = JSON.generate({:msgType => msg_type, :data => data})
    else
      msg = JSON.generate({:msgType => msg_type, :data => data, :gameTick => game_tick})
    end
    if (@config[:debug]) then
      transcript(msg_type, data, :to)
    end
    return msg
  end
end
