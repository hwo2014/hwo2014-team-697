# (c) Team Doge, 2014
# http://www.apache.org/licenses/LICENSE-2.0.html 

class TrackMetrics
  
  attr_reader :cars, :race, :track, :pieces, :lanes
  
  def initialize(racedata)
    @cars = racedata["cars"]
    @race = racedata["raceSession"]
    @track = racedata["track"]
    @lanes = Hash[racedata["track"]["lanes"].collect {|lane|
      [lane["index"], lane["distanceFromCenter"]]
      }]
    @pieces = parse_pieces(racedata["track"]["pieces"])
  end
  
  def lane_count
    @lane_count ||= @lanes.size
  end
  
  # Distance from piece_index to the next bend
  # FIXME: Doesn't account for switches
  def distance_to_bend(piece_index, in_piece_distance, lane)
    return 0 if @pieces[piece_index].type == :bend
    first_bend = @pieces[piece_index..@pieces.size-1].select {|piece| piece.type == :bend}.first
    first_bend = @pieces[0...piece_index].select {|piece| piece.type == :bend}.first unless first_bend # roll over
    raise "Track has no bends?!" unless first_bend
    # puts "Next bend at #{first_bend.index}"
    distance_to_piece(piece_index, in_piece_distance, lane, first_bend.index)
  end
  
  # Distance from piece_index to piece_index2
  # FIXME: Doesn't account for switches
  def distance_to_piece(piece_index, in_piece_distance, lane, piece_index2)
    # puts "calculating from #{piece_index}.#{in_piece_distance} to #{piece_index2} on lane #{lane}"
    return 0 if (piece_index2 == piece_index) && in_piece_distance == 0 
    if piece_index2 <= piece_index
      # puts "Rolling over"
      distance_to_end = distance_to_end(piece_index, in_piece_distance, lane)
      # puts "Distance to end #{distance_to_end}"
      distance_from_start = distance_to_piece(0, 0, lane, piece_index2)
      # puts "Distance from start #{distance_from_start}"
      return distance_to_end + distance_from_start
    end
    remaining_distance_on_piece = @pieces[piece_index].length(lane)-in_piece_distance
    distance = @pieces[piece_index+1...piece_index2].inject(0) {|sum, piece| sum + piece.length(lane)}
    distance + remaining_distance_on_piece
  end
  
  def distance_to_end(piece_index, in_piece_distance, lane)
    # distance_to_last piece
    unless piece_index == @pieces.last.index
      distance_to_last_piece = distance_to_piece(piece_index, in_piece_distance, lane, @pieces.size-1)
      distance_to_end = distance_to_last_piece + @pieces.last.length(lane)
    else
      # We're on the last piece
      distance_to_end = @pieces.last.length(lane) - in_piece_distance
    end
    return distance_to_end
  end
  
  def laps
    @race["laps"]
  end
  
  private
  
  def parse_pieces(pieces)
    pieces.collect.with_index {|piece, i|
      Piece.new(piece, i, @lanes)
    }
  end
  
end

class Piece
  
  attr_reader :index, :type, :length, :radius, :angle, :switch
  
  def initialize(piece, i, lanes)
    @index = i
    if piece["length"].nil? then 
      @type = :bend
      @radius = piece["radius"]
      @angle = piece["angle"]
    else 
      @type = :straight
      @length = piece["length"]
    end
    @switch = if piece["switch"] then true else false end
    @lanes = lanes
    @lane_length = Hash.new
  end
  
  # Calculates the length of a lane in a piece
  # if lane2 is defined and the piece is a switch, calculates
  # the length of switching from lane to lane2
  def length(lane = 0, lane2 = nil)
    lane2 = nil if lane == lane2 # not really a switch

    @lane_length[[lane, lane2]] ||= case @type
    when :bend
      unless lane2
        offset = @lanes[lane]
        if (@angle > 0) then
          r = @radius - offset
        else
          r = @radius + offset
        end
        # arc distance
        length = (2*r*Math::sin(@angle/2.0*Math::PI/180.0)).abs        
      else
        switch_offset = @lanes[lane2] - @lanes[lane] # abs(lane-lane2) == 1
        # lane starts at (0,0), lane2 ends at (x,y) and starts at (-switch_offset,0)
        offset = @lanes[lane2]
        o = @angle
        x = -switch_offset
        y = 0
        a = 0
        
        if (o > 0) then
          r = @radius - offset
        else
          r = @radius + offset
        end
        
        if (o > 0) then
          # turn to right
          b = a - o
        else
          # turn to left
    #      arc.stroke = "red"
          a = a + 180
          b = a + o.abs
        end
        
        xo = x - r * Math::cos(a*Math::PI/180.0)
        yo = y + r * Math::sin(a*Math::PI/180.0)
      
        # B = (x1,y1)
        x1 = xo + r * Math::cos(b*Math::PI/180.0)
        y1 = yo - r * Math::sin(b*Math::PI/180.0)
        # pp "Drawing from (0,0) to (#{x1},#{y1}) lane #{lane} -> #{lane2}"
        # pp "#{lane2} origin (#{x},#{y})"
        
        # FIXME: This assumes the switch is totally straight...
        length = Math::sqrt(x1**2 + y1**2) # Pythagoras action

      end
    else
      unless lane2
        @length # length is already defined in init for straights
      else
        offset = @lanes[lane] - @lanes[lane2] # abs(lane-lane2) == 1
        # In reality, the length seems to be a bit longer
        # Note the curves in tech spec visualisations
        length = Math::sqrt(offset**2 + @length**2) # Pythagoras action
      end
    end
      
  end
  
end
